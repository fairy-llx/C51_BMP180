/*************************************************************
                      BMP180头文件

实现功能：BMP180的控制

补充说明：
***************************************************************/
#ifndef _BMP180_H_
#define _BMP180_H_
#include <reg52.H>
#include <LCD1602.h>
#include <intrins.H>
#include <math.h>

#define uchar unsigned char
#define uint unsigned int
#define PRESSURE_0 101325L //  标准大气压

/*****************BMP180引脚定义*******************/
sbit BMP180_SCL = P3 ^ 4; // IIC时钟引脚定义
sbit BMP180_SDA = P3 ^ 3; // IIC数据引脚定义

/*****************BMP180变量定义*******************/
#define BMP085_SlaveAddress 0xee       //定义器件在IIC总线中的从地址
uchar ge, shi, bai, qian, wan, shiwan; //显示变量

short ac1;
short ac2; 
short ac3; 
unsigned short ac4;
unsigned short ac5;
unsigned short ac6;
short b1; 
short b2;
short mb;
short mc;
short md;

uchar temp_z, atmos_z, alti_z; //温度、大气压、海拔整数部分
uchar temp_x, atmos_x, alti_x; //温度、大气压、海拔小数部分
bit alti_flag = 0;
/*****************BMP180函数定义*******************/
void delayms(uint k);                   //毫秒延时函数
void Delay5us();                        // 5us延时函数
void BMP085_Start();                    // BMP180起始信号
void BMP085_Stop();                     // BMP180停止信号
void BMP085_SendACK(bit ack);           //发送应答信号
bit BMP085_RecvACK();                   //接收应答信号
void BMP085_SendByte(uchar dat);        //发送一个字节数据
uchar BMP085_RecvByte();                //接收一个字节数据
short Multiple_read(uchar ST_Address);  //读取BMP180内部信息
long BMP180_ReadTemp(void);             // BMP180读取温度
long BMP180_ReadPressure(void);         // BMP180读取大气压
void BMP180_init();                     // BMP180初始化
void BMP180_conversion(long temp_data); // BMP180数据转换函数
void BMP180_Convert();                  // BMP180读取并转化

/********************************************************
函数名称:void delayms(uint k)
函数作用:毫秒延时函数
参数说明:K：表示延时几MS
********************************************************/
void delayms(uint k)
{
    uint i, j;
    for (i = 0; i < k; i++)
    {
        for (j = 0; j < 121; j++)
        {
            ;
        }
    }
}

/********************************************************
函数名称:void Delay5us()
函数作用:5us延时函数
参数说明:
********************************************************/
void Delay5us()
{
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    _nop_();
}

/********************************************************
函数名称:void BMP085_Start()
函数作用:IIC起始信号
参数说明:
********************************************************/
void BMP085_Start()
{
    BMP180_SDA = 1; //拉高数据线
    BMP180_SCL = 1; //拉高时钟线
    Delay5us();     //延时
    BMP180_SDA = 0; //产生下降沿
    Delay5us();     //延时
    BMP180_SCL = 0; //拉低时钟线
}

/********************************************************
函数名称:void BMP085_Stop()
函数作用:IIC停止信号
参数说明:
********************************************************/
void BMP085_Stop()
{
    BMP180_SDA = 0; //拉低数据线
    BMP180_SCL = 1; //拉高时钟线
    Delay5us();     //延时
    BMP180_SDA = 1; //产生上升沿
    Delay5us();     //延时
}

/********************************************************
函数名称:void BMP085_SendACK(bit ack)
函数作用:发送应答信号
参数说明:1：不应答。0：应答
********************************************************/
void BMP085_SendACK(bit ack)
{
    BMP180_SDA = ack; //写应答信号
    BMP180_SCL = 1;   //拉高时钟线
    Delay5us();       //延时
    BMP180_SCL = 0;   //拉低时钟线
    Delay5us();       //延时
}

/********************************************************
函数名称:bit BMP085_RecvACK()
函数作用:接收应答信号
参数说明:接收到的1：不应答。0：应答
********************************************************/
bit BMP085_RecvACK()
{
    BMP180_SCL = 1;  //拉高时钟线
    Delay5us();      //延时
    CY = BMP180_SDA; //读应答信号
    BMP180_SCL = 0;  //拉低时钟线
    Delay5us();      //延时
    return CY;
}

/********************************************************
函数名称:void BMP085_SendByte(uchar dat)
函数作用:向IIC发送一个字节数据
参数说明:
********************************************************/
void BMP085_SendByte(uchar dat)
{
    uchar i;
    for (i = 0; i < 8; i++) // 8位计数器
    {
        dat <<= 1;       //移出数据的最高位
        BMP180_SDA = CY; //送数据口
        BMP180_SCL = 1;  //拉高时钟线
        Delay5us();      //延时
        BMP180_SCL = 0;  //拉低时钟线
        Delay5us();      //延时
    }
    BMP085_RecvACK();
}

/********************************************************
函数名称:uchar BMP085_RecvByte()
函数作用:向IIC读取一个字节数据
参数说明:
********************************************************/
uchar BMP085_RecvByte()
{
    uchar i;
    uchar dat = 0;
    BMP180_SDA = 1;         //使能内部上拉,准备读取数据,
    for (i = 0; i < 8; i++) // 8位计数器
    {
        dat <<= 1;
        BMP180_SCL = 1;    //拉高时钟线
        Delay5us();        //延时
        dat |= BMP180_SDA; //读数据
        BMP180_SCL = 0;    //拉低时钟线
        Delay5us();        //延时
    }
    return dat;
}

/********************************************************
函数名称:short Multiple_read(uchar ST_Address)
函数作用:向BMP180内部读取数据
参数说明:
********************************************************/
short Multiple_read(uchar ST_Address)
{
    uchar msb, lsb;
    short _data;
    BMP085_Start();                           //起始信号
    BMP085_SendByte(BMP085_SlaveAddress);     //发送设备地址+写信号
    BMP085_SendByte(ST_Address);              //发送存储单元地址
    BMP085_Start();                           //起始信号
    BMP085_SendByte(BMP085_SlaveAddress + 1); //发送设备地址+读信号
    msb = BMP085_RecvByte();                  // BUF[0]存储
    BMP085_SendACK(0);                        //回应ACK
    lsb = BMP085_RecvByte();
    BMP085_SendACK(1); //最后一个数据需要回NOACK
    BMP085_Stop();     //停止信号
    delayms(5);
    _data = msb << 8;
    _data |= lsb;
    return _data;
}

/********************************************************
函数名称:long BMP180_ReadTemp(void)
函数作用:向BMP180内部读取温度
参数说明:
********************************************************/
long BMP180_ReadTemp(void)
{
    BMP085_Start();                       //起始信号
    BMP085_SendByte(BMP085_SlaveAddress); //发送设备地址+写信号
    BMP085_SendByte(0xF4);                //写寄存器地址
    BMP085_SendByte(0x2E);                //写温度寄存器地址
    BMP085_Stop();                        //发送停止信号
    delayms(10);                          //延时
    return (long)Multiple_read(0xF6);
}

/********************************************************
函数名称:long BMP180_ReadPressure(void)
函数作用:向BMP180内部读取大气压
参数说明:
********************************************************/
long BMP180_ReadPressure(void)
{
    long pressure = 0;
    BMP085_Start();                       //起始信号
    BMP085_SendByte(BMP085_SlaveAddress); //发送设备地址+写信号
    BMP085_SendByte(0xF4);                //写寄存器地址
    BMP085_SendByte(0x34);                //写压力寄存器地址
    BMP085_Stop();                        //发送停止信号
    delayms(10);                          //延时
    pressure = Multiple_read(0xF6);       //读取压力数据
    pressure &= 0x0000FFFF;
    return pressure;
}

/********************************************************
函数名称:void BMP180_init()
函数作用:BMP180初始化函数
参数说明:
********************************************************/
void BMP180_init()
{
	ac1 = Multiple_read(0xAA);	//获取校准参数
	ac2 = Multiple_read(0xAC);
	ac3 = Multiple_read(0xAE);
	ac4 = Multiple_read(0xB0);
	ac5 = Multiple_read(0xB2);
	ac6 = Multiple_read(0xB4);
	b1 =  Multiple_read(0xB6);
	b2 =  Multiple_read(0xB8);
	mb =  Multiple_read(0xBA);
	mc =  Multiple_read(0xBC);
	md =  Multiple_read(0xBE);
}

/********************************************************
函数名称:void BMP180_conversion(long temp_data)
函数作用:BMP180数据转换函数
参数说明:将从BMP180读出的 将数据各个位提取出来
********************************************************/
void BMP180_conversion(long temp_data)
{
    shiwan = temp_data / 100000;
    temp_data = temp_data % 100000; //取余运算
    wan = temp_data / 10000;
    temp_data = temp_data % 10000; //取余运算
    qian = temp_data / 1000;
    temp_data = temp_data % 1000; //取余运算
    bai = temp_data / 100;
    temp_data = temp_data % 100; //取余运算
    shi = temp_data / 10;
    temp_data = temp_data % 10; //取余运算
    ge = temp_data;
}

/********************************************************
函数名称:void BMP180_Convert()
函数作用:BMP180读取并显示显示
参数说明:
********************************************************/
void BMP180_Convert()
{
    long ut;
    long up;
    long x1, x2, b5, b6, x3, b3, p;
    unsigned long b4, b7;
    long value_;

    ut = BMP180_ReadTemp();
    ut = BMP180_ReadTemp(); // 读取温度
    up = BMP180_ReadPressure();
    up = BMP180_ReadPressure(); // 读取压强

//    x1 = ((long)ut - 23153) * 32757 >> 15;
//    x2 = (-17840128) / (x1 + 2868);
//    // x2 = ((long)-8711 << 11) / (x1 + 2868);
//    b5 = x1 + x2;
//    value_ = (b5 + 8) >> 4;
    
    
	x1 = ((long)ut - ac6) * ac5 >> 15;
	x2 = ((long) mc << 11) / (x1 + md);
	b5 = x1 + x2;
	value_ = (b5 + 8) >> 4;	

    BMP180_conversion(value_); //转换温度数据各个位提取出来

    temp_z = bai * 10 + shi; //提取温度整数部分
    temp_x = ge;             //提取温度小数部分

    //*************
//    b6 = b5 - 4000;
//    x1 = (4 * (b6 * b6 >> 12)) >> 11;
//    x2 = -72 * b6 >> 11;
//    x3 = x1 + x2;
//    b3 = (((long)1632 + x3) + 2) / 4;
//    // b3 = (((long)408 * 4 + x3) + 2) / 4;
//    x1 = -14383 * b6 >> 13;
//    x2 = (6190 * (b6 * b6 >> 12)) >> 16;
//    x3 = ((x1 + x2) + 2) >> 2;
//    b4 = (32741 * (unsigned long)(x3 + 32768)) >> 15;
//    b7 = ((unsigned long)up - b3) * (50000 >> 0);
//    if (b7 < 0x80000000)
//        p = (b7 * 2) / b4;
//    else
//        p = (b7 / b4) * 2;
//    x1 = (p >> 8) * (p >> 8);
//    x1 = (x1 * 3038) >> 16;
//    x2 = (-7357 * p) >> 16;
//    value_ = p + ((x1 + x2 + 3791) >> 4);
    b6 = b5 - 4000;
	x1 = (b2 * (b6 * b6 >> 12)) >> 11;
	x2 = ac2 * b6 >> 11;
	x3 = x1 + x2;
	b3 = (((long)ac1 * 4 + x3) + 2)/4;
	x1 = ac3 * b6 >> 13;
	x2 = (b1 * (b6 * b6 >> 12)) >> 16;
	x3 = ((x1 + x2) + 2) >> 2;
	b4 = (ac4 * (unsigned long) (x3 + 32768)) >> 15;
	b7 = ((unsigned long) up - b3) * (50000 >>0);
	if( b7 < 0x80000000)
		p = (b7 * 2) / b4 ;
	else  
		p = (b7 / b4) * 2;
	x1 = (p >> 8) * (p >> 8);
	x1 = (x1 * 3038) >> 16;
	x2 = (-7357 * p) >> 16;
	value_ = p + ((x1 + x2 + 3791) >> 4);
    
    BMP180_conversion(value_); //转换大气压数据各个位提取出来

    atmos_z = shiwan * 100 + wan * 10 + qian; //提取大气压整数部分
    atmos_x = bai * 10 + shi;                 //提取大气压小数部分

    if(value_ > PRESSURE_0) //  气压大于标准大气压，海拔低于海平面
    {
        alti_flag = 0;      // 表示负海拔
    }
    else
    {
        alti_flag = 1;
    }

    value_ = (443300.0 * (1.0 - pow((value_ * 1.0 / PRESSURE_0), (1.0 / 5.255)))); // 单位：0.1米

    BMP180_conversion(value_); //转换海拔数据各个位提取出来

    alti_z = qian + wan * 10; //提取海拔整数部分
    alti_x = shi + bai * 10;  //提取海拔小数部分
}
#endif