/***************************************************
 *@Author       : fairy
 *@Date         : 2022-04-12 22:15:56
*@LastEditors  : fairy
*@LastEditTime : 2022-04-13 11:17:13
*@FilePath     : main.c
 ***************************************************/
#include <BMP180.h>
#include <LCD1602.h>

sbit beep = P2 ^ 3; // 蜂鸣器引脚
sbit led1 = P3 ^ 6; // LED 1 引脚，气压异常指示灯
sbit led2 = P3 ^ 7; // LED 2 引脚，温度异常指示灯

unsigned char sys_timer = 0;  // 计时器，每10ms加一，用于计时
unsigned char sys_timer_ = 0; // 计时器，每500ms加一，用于设置界面时显示的闪烁
bit gFlag_10ms = 0;           // 标志位，每10ms置一，用于按键扫描
bit gFlag_500ms = 1;          // 标志位，每500ms置一，用于刷新显示内容
unsigned char keyvalue;       // 键值，用于判断按下的按键
unsigned char intreface = 0;  // 界面索引，0表示正常显示界面
                              // 1~4表示设置界面，分别是温度上限、温度下限、大气压上限、大气压下限的设置

unsigned char threshold[4] = {30, 20, 105, 90}; //  阈值数组，依次是温度上限、温度下限、大气压上限、大气压下限

void Timer1Init(void);       // 定时器1初始化函数
unsigned char KeyScan(void); // 按键扫描函数，采用中断的方式扫描，优点是效率高，不会阻塞主函数

void main(void)
{
    beep = 0; // 蜂鸣器不响
    led1 = 1; // 灯灭
    led2 = 1; // 灯灭
                
    LCD_init(); // 液晶初始化
    {           //  显示固定字符
        LCD_disp_char(2, 1, 'P');
        LCD_disp_char(3, 1, ':');
        LCD_disp_char(7, 1, '.');
        LCD_disp_char(10, 1, 'k');
        LCD_disp_char(11, 1, 'P');
        LCD_disp_char(12, 1, 'a');

        LCD_disp_char(0, 2, 'T');
        LCD_disp_char(1, 2, ':');
        LCD_disp_char(4, 2, '.');
        LCD_disp_char(6, 2, 'C');
        LCD_disp_char(8, 2, 'A');
        LCD_disp_char(9, 2, ':');
        LCD_disp_char(15, 2, 'm');
    }
    BMP180_init(); // BMP180初始化
    Timer1Init();  // 定时器1初始化

    while (1)
    {
        // 每10ms扫描一次按键
        if (gFlag_10ms)
        {
            gFlag_10ms = 0;
            keyvalue = KeyScan();
        }

        // 每500ms执行一次，刷新显示
        if (gFlag_500ms)
        {
            gFlag_500ms = 0;
            if (intreface == 0) // 正常显示界面
            {
                BMP180_Convert(); //  BMP180读取、转换数据
                // 显示大气压的整数部分
                LCD_disp_char(4, 1, '0' + atmos_z / 100 % 10);
                LCD_disp_char(5, 1, '0' + atmos_z / 10 % 10);
                LCD_disp_char(6, 1, '0' + atmos_z % 10);
                // 显示大气压的小数部分
                LCD_disp_char(8, 1, '0' + atmos_x / 10 % 10);
                LCD_disp_char(9, 1, '0' + atmos_x % 10);
                // 显示温度的整数部分
                LCD_disp_char(2, 2, '0' + temp_z / 10 % 10);
                LCD_disp_char(3, 2, '0' + temp_z % 10);
                // 显示温度的小数部分
                LCD_disp_char(5, 2, '0' + temp_x % 10);
                // 显示海拔的符号（根据气压与标准大气压的比较）
                LCD_disp_char(10, 2, (alti_flag ? '+' : '-'));
                // 显示大气压的前两位数字
                LCD_disp_char(11, 2, '0' + alti_z / 10 % 10);
                LCD_disp_char(12, 2, '0' + alti_z % 10);
                // 显示大气压的后两位数字
                LCD_disp_char(13, 2, '0' + alti_x / 10 % 10);
                LCD_disp_char(14, 2, '0' + alti_x % 10);

                // 判断大气压是否在设置的阈值之间
                if ((atmos_z >= threshold[2]) || (atmos_z < threshold[3])) //  大气压数据不在阈值之间
                {
                    led1 = 0; // 灯亮
                }
                else //  大气压数据在阈值之间
                {
                    led1 = 1; // 灯灭
                }

                // 判断温度是否在设置的阈值之间
                if ((temp_z >= threshold[0]) || (temp_z < threshold[1])) //  温度数据不在阈值之间
                {
                    led2 = 0; // 灯亮
                }
                else //  温度数据在阈值之间
                {
                    led2 = 1; // 灯灭
                }

                // 处理蜂鸣器
                if ((led1 == 0) || (led2 == 0)) // led1 或者 led2 任意一个（或两个）亮了，蜂鸣器均报警
                {
                    beep = 1;   // 蜂鸣器响
                }
                else
                {
                    beep = 0; // 蜂鸣器不响
                }
            }
            else //  设置界面
            {
                led1 = 1; // 灯灭
                led2 = 1; // 灯灭
                beep = 0; // 蜂鸣器不响
                // 注释掉的部分是正常显示的，但是当前设置项不会闪烁，交互性不强
                // 显示的原理很简单，调用函数，输入显示的位置和需要显示的字符即可
                // LCD_disp_char(5, 1, '0' + threshold[0] / 10 % 10);
                // LCD_disp_char(6, 1, '0' + threshold[0] % 10);

                // LCD_disp_char(12, 1, '0' + threshold[1] / 10 % 10);
                // LCD_disp_char(13, 1, '0' + threshold[1] % 10);

                // LCD_disp_char(5, 2, '0' + threshold[2] / 100 % 10);
                // LCD_disp_char(6, 2, '0' + threshold[2] / 10 % 10);
                // LCD_disp_char(7, 2, '0' + threshold[2] % 10);

                // LCD_disp_char(12, 2, '0' + threshold[3] / 100 % 10);
                // LCD_disp_char(13, 2, '0' + threshold[3] / 10 % 10);
                // LCD_disp_char(14, 2, '0' + threshold[3] % 10);

                // 这段可以实现闪烁提示当前项目的效果，演示视频有的。
                // 显示还是调用那个函数，显示的内容变了而已。
                /*
                    举个例子

                    unsigned char intreface = 0;  // 界面索引，0表示正常显示界面
                              // 1~4表示设置界面，分别是温度上限、温度下限、大气压上限、大气压下限的设置

                    当intreface = 1 表示温度上限的设置
                    所以如果需要设置温度上限的时候，显示的内容就需要闪烁，其余界面（2，3，4）不闪烁
                    (intreface == 1)        // 三木运算符，判断当前的界面是不是1，是1就需要闪烁，不是1就无需闪烁
                                        ?
                                            ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[0] / 10 % 10))    //  是1 的情况，需要闪烁
                                        :
                                            ('0' + threshold[0] / 10 % 10)) //  不是1的情况，正常显示即可

                    需要闪烁的情况：
                    (
                        (sys_timer_ % 2 == 0)       sys_timer_这个变量就是用于闪烁的，sys_timer_每500ms加一，所以就有了“间隔”
                                                ?
                                                    (' ')   偶数时显示空白
                                                :
                                                    ('0' + threshold[0] / 10 % 10))  奇数时正常显示

                    看起来的效果就是在闪烁了
                */
                LCD_disp_char(5, 1, (intreface == 1) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[0] / 10 % 10)) : ('0' + threshold[0] / 10 % 10));
                LCD_disp_char(6, 1, (intreface == 1) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[0] % 10)) : ('0' + threshold[0] % 10));

                LCD_disp_char(12, 1, (intreface == 2) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[1] / 10 % 10)) : ('0' + threshold[1] / 10 % 10));
                LCD_disp_char(13, 1, (intreface == 2) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[1] % 10)) : ('0' + threshold[1] % 10));

                LCD_disp_char(5, 2, (intreface == 3) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[2] / 100 % 10)) : ('0' + threshold[2] / 100 % 10));
                LCD_disp_char(6, 2, (intreface == 3) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[2] / 10 % 10)) : ('0' + threshold[2] / 10 % 10));
                LCD_disp_char(7, 2, (intreface == 3) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[2] % 10)) : ('0' + threshold[2] % 10));

                LCD_disp_char(12, 2, (intreface == 4) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[3] / 100 % 10)) : ('0' + threshold[3] / 100 % 10));
                LCD_disp_char(13, 2, (intreface == 4) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[3] / 10 % 10)) : ('0' + threshold[3] / 10 % 10));
                LCD_disp_char(14, 2, (intreface == 4) ? ((sys_timer_ % 2 == 0) ? (' ') : ('0' + threshold[3] % 10)) : ('0' + threshold[3] % 10));
            }
        }

        //  处理按键
        if (keyvalue)
        {
            // LCD_disp_char(15, 1, '0' + keyvalue % 10);
            switch (keyvalue)
            {
            case 1: //  设置
                if (intreface == 0)
                {
                    led1 = 1;
                    led2 = 1;
                    beep = 1;
                }
                intreface++;
                if (intreface > 4)
                {
                    intreface = 0;
                }
                // 从设置界面回到正常显示界面，把固定内容先显示出来
                if (intreface == 0)
                {
                    LCD_disp_char(0, 1, ' ');
                    LCD_disp_char(1, 1, ' ');
                    LCD_disp_char(2, 1, 'P');
                    LCD_disp_char(3, 1, ':');
                    LCD_disp_char(7, 1, '.');
                    LCD_disp_char(10, 1, 'k');
                    LCD_disp_char(11, 1, 'P');
                    LCD_disp_char(12, 1, 'a');
                    LCD_disp_char(13, 1, ' ');
                    LCD_disp_char(14, 1, ' ');
                    LCD_disp_char(15, 1, ' ');

                    LCD_disp_char(0, 2, 'T');
                    LCD_disp_char(1, 2, ':');
                    LCD_disp_char(4, 2, '.');
                    LCD_disp_char(6, 2, 'C');
                    LCD_disp_char(7, 2, ' ');
                    LCD_disp_char(8, 2, 'A');
                    LCD_disp_char(9, 2, ':');
                    LCD_disp_char(15, 2, 'm');
                }
                // 从正常显示界面切换到设置界面，先显示固定内容
                else if (intreface == 1)
                {
                    //  1234567822345678
                    //  T up:85C dn:25c
                    //  p up:110 dn: 30
                    LCD_disp_char(0, 1, 'T');
                    LCD_disp_char(1, 1, ' ');
                    LCD_disp_char(2, 1, 'U');
                    LCD_disp_char(3, 1, 'p');
                    LCD_disp_char(4, 1, ':');
                    LCD_disp_char(7, 1, 'C');
                    LCD_disp_char(8, 1, ' ');
                    LCD_disp_char(9, 1, 'D');
                    LCD_disp_char(10, 1, 'n');
                    LCD_disp_char(11, 1, ':');
                    LCD_disp_char(14, 1, 'C');
                    LCD_disp_char(15, 1, ' ');

                    LCD_disp_char(0, 2, 'P');
                    LCD_disp_char(1, 2, ' ');
                    LCD_disp_char(2, 2, 'U');
                    LCD_disp_char(3, 2, 'P');
                    LCD_disp_char(4, 2, ':');
                    LCD_disp_char(8, 2, ' ');
                    LCD_disp_char(9, 2, 'D');
                    LCD_disp_char(10, 2, 'n');
                    LCD_disp_char(11, 2, ':');

                    LCD_disp_char(15, 2, ' ');
                }
                break;

            case 2:                //  加
                switch (intreface) //  根据界面的不同，处理不同的数据
                {
                case 1:                    //  温度上限
                    if (threshold[0] < 85) //  BMP180的温度测量范围上限是85
                        threshold[0]++;
                    break;
                case 2:                              //  温度下限
                    if (threshold[1] < threshold[0]) // 温度下限要比温度上限小
                        threshold[1]++;
                    break;
                case 3:                     //  大气压上限
                    if (threshold[2] < 110) //  BMP180的气压测量范围上限是110kPa
                        threshold[2]++;
                    break;
                case 4:                              //  大气压下限
                    if (threshold[3] < threshold[2]) // 大气压下限要比大气压上限小
                        threshold[3]++;
                    break;
                }
                break;

            case 3: //  减
                switch (intreface)
                {
                case 1: //  温度上限
                    if (threshold[0] > threshold[1])
                        threshold[0]--;
                    break;
                case 2: //  温度下限
                    if (threshold[1] > 0)
                        threshold[1]--;
                    break;
                case 3: //  大气压上限
                    if (threshold[2] > threshold[3])
                        threshold[2]--;
                    break;
                case 4: //  大气压下限
                    if (threshold[3] > 30)
                        threshold[3]--;
                    break;
                }
                break;
            }
            keyvalue = 0; // 因为是中断扫描的按键处理，所以需要把键值赋值为无效键值
        }
    }
}

/***************************************************
 *@description  : 定时器1 中断函数，每10ms自动执行一次
 *@param         {*}
 *@return        {*}
 ***************************************************/
void Timer1Interrupt(void) interrupt 3
{
    TF1 = 0;    //清除TF1标志
    TL1 = 0xF0; //设置定时初始值
    TH1 = 0xD8; //设置定时初始值    //  10MS

    sys_timer++;

    gFlag_10ms = 1; // 10ms标志位

    if (sys_timer > 250) //  处理溢出
    {
        sys_timer = 0;
    }

    if (sys_timer % 50 == 0) //  500ms标志位
    {
        gFlag_500ms = 1;
        sys_timer_++; //  用于设置界面时显示的闪烁
    }
}

/***************************************************
 *@description  : 定时器1 初始化 1毫秒@12.000MHz
 *@param         {*}
 *@return        {*}
 ***************************************************/
void Timer1Init(void)
{
    TMOD &= 0x0F; //设置定时器模式
    TMOD |= 0x10; //设置定时器模式
    TL1 = 0xF0;   //设置定时初始值
    TH1 = 0xD8;   //设置定时初始值    //  10MS
    TF1 = 0;      //清除TF1标志
    TR1 = 1;      //定时器1开始计时
    ET1 = 1;      //定时器1中断开
    EA = 1;       //总中断开
}

/***************************************************
 *@description  : 按键扫描函数
 *@param         {*}
 *@return        {*}
 ***************************************************/
unsigned char KeyScan(void)
{
    static unsigned char KeyPress = 0;    // 按键检测标志位，每10ms扫描一次，检测到按键有效则加一
    static unsigned char KeyFree = 0;     // 按键释放标志位，按键按下后置一，表示按键已经按键可以释放
    static unsigned char KeyValueTem = 0; // 按键键值缓存值
    unsigned char KeyValue;               // 实际返回的键值

    // 先让按键输出高电平
    P1 = (P1 & 0xB6) | 0x49;

    // 检测按键管脚的状态
    if ((P1 & 0x49) != 0x49) // 有按键按下
    {
        KeyPress++; // 按键标志位+1
    }
    else // 无按键按下
    {
        KeyPress = 0; // 按键标志位复位
    }

    if (KeyPress > 3) // 按键标志位
    {
        KeyFree = 1; // 按键释放标志位置一，表示按键有效按下，等待按键释放时即返回键值
        switch (P1 & 0x49)
        {
        case 0x48:
            KeyValueTem = 1;
            break;
        case 0x41:
            KeyValueTem = 2;
            break;
        case 0x09:
            KeyValueTem = 3;
            break;
        default:
            KeyValueTem = 0;
            break;
        }
    }

    // 无按键按下 && 按键按下过  == 按键松手
    if (((P1 & 0x49) == 0x49) && (KeyFree == 1))
    {
        KeyFree = 0;            // 按键释放标志位复位
        KeyPress = 0;           // 标志位复位
        KeyValue = KeyValueTem; // 键值缓存赋值给实际返回的键值
        KeyValueTem = 0;        // 键值缓存清除
    }
    else
    {
        KeyValue = 0; // 无效键值
    }

    return KeyValue;
}