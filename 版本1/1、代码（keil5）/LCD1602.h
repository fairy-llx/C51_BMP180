/***************************************************
*@Author       : fairy
*@Date         : 2021-09-17 10:55:00
*@LastEditors  : fairy
*@LastEditTime : 2022-03-13 09:33:50
*@FilePath     : LCD1602.h
***************************************************/
#ifndef __LCD1602__
#define __LCD1602__

#include <reg51.h>

#define LCD1602_DB P0	//LCD1602数据总线

sbit LCD1602_RS = P3^5;	 //RS端
sbit LCD1602_RW = P3^6;	 //RW端
sbit LCD1602_EN = P3^4;	 //EN端
sbit DU = P2^6;//
sbit WE = P2^7;//数码管位选段选用于关闭数码管显示

void Read_Busy(void);
void LCD1602_Write_Cmd(unsigned char cmd);
void LCD1602_Write_Dat(unsigned char dat);
void LCD1602_Dis_OneChar(unsigned char x, unsigned char y,unsigned char dat);
void LCD1602_Dis_Str(unsigned char x, unsigned char y, unsigned char *str);
void Init_LCD1602(void);

#endif