/***************************************************
 *@Author       : fairy
 *@Date         : 2022-03-12 14:42:24
*@LastEditors  : fairy
*@LastEditTime : 2022-03-13 11:27:51
*@FilePath     : BMP180.c
 ***************************************************/

#include "BMP180.h"

sbit SCL = P2 ^ 1; // IIC时钟引脚定义
sbit SDA = P2 ^ 0; // IIC数据引脚定义

#define BMP180_SlaveAddress 0xEE //定义器件在IIC总线中的从地址 BMP180=0xee,BMP280=0xec

#define OSS 0 // Oversampling Setting (note: code is not set up to use other OSS values)

#define PRESSURE_0 101325L //  标准大气压

//  BMP180 校准系数
short ac1;
short ac2;
short ac3;
unsigned short ac4;
unsigned short ac5;
unsigned short ac6;
short b1;
short b2;
short mb;
short mc;
short md;

long temperature = 0; // 实际温度数据，单位：0.1 摄氏度
long pressure = 0;    // 实际压强数据，单位：1   帕斯卡
long altitude = 0;    // 根据气压计算出来的海拔高度，单位：1 厘米（计算方便）

/***************************************************
 *@description  : IIC总线 延时5us
 *@param         {*}
 *@return        {*}
 ***************************************************/
void Delay5us()
{
}

/***************************************************
 *@description  : 延时5ms
 *@param         {*}
 *@return        {*}
 ***************************************************/
void Delay5ms()
{
    unsigned char i, j;
    i = 9;
    j = 244;
    do
    {
        while (--j)
            ;
    } while (--i);
}

/***************************************************
 *@description  : 延时函数，单位ms
 *@param         {unsigned int} k 延时时间
 *@return        {*}
 ***************************************************/
void delay(unsigned int k)
{
    unsigned int i, j;
    for (i = 0; i < k; i++)
    {
        for (j = 0; j < 121; j++)
        {
            ;
        }
    }
}

/***************************************************
 *@description  : IIC总线 起始信号
 *@param         {*}
 *@return        {*}
 ***************************************************/
void BMP180_Start()
{
    SDA = 1;    //拉高数据线
    SCL = 1;    //拉高时钟线
    Delay5us(); //延时
    SDA = 0;    //产生下降沿
    Delay5us(); //延时
    SCL = 0;    //拉低时钟线
}

/***************************************************
 *@description  : IIC总线 停止信号
 *@param         {*}
 *@return        {*}
 ***************************************************/
void BMP180_Stop()
{
    SDA = 0;    //拉低数据线
    SCL = 1;    //拉高时钟线
    Delay5us(); //延时
    SDA = 1;    //产生上升沿
    Delay5us(); //延时
}

/***************************************************
 *@description  : IIC总线 发送应答信号
 *@param         {bit} ack [0:ACK] [1:NOACK]
 *@return        {*}
 ***************************************************/
void BMP180_SendACK(bit ack)
{
    SDA = ack;  //写应答信号
    SCL = 1;    //拉高时钟线
    Delay5us(); //延时
    SCL = 0;    //拉低时钟线
    Delay5us(); //延时
}

/***************************************************
 *@description  : IIC总线 接收应答信号
 *@param         {*}
 *@return        {*}
 ***************************************************/
bit BMP180_RecvACK()
{
    bit CY;
    SCL = 1;    //拉高时钟线
    Delay5us(); //延时
    CY = SDA;   //读应答信号
    SCL = 0;    //拉低时钟线
    Delay5us(); //延时

    return CY;
}

/***************************************************
 *@description  : IIC总线 发送一个字节数据
 *@param         {unsigned char} dat
 *@return        {*}
 ***************************************************/
void BMP180_SendByte(unsigned char dat)
{
    unsigned char i;

    for (i = 0; i < 8; i++) // 8位计数器
    {
        SCL = 0;        //拉低时钟线
        Delay5us();     //延时
        if (dat & 0x80) //数据
            SDA = 1;
        else
            SDA = 0;
        Delay5us(); //延时
        SCL = 1;    //拉高时钟线
        dat <<= 1;
        Delay5us(); //延时
    }
    SCL = 0; //拉低时钟线
    BMP180_RecvACK();
}

/***************************************************
 *@description  : IIC总线 接收一个字节数据
 *@param         {*}
 *@return        {*}
 ***************************************************/
unsigned char BMP180_RecvByte()
{
    unsigned char i;
    unsigned char dat = 0;
    SDA = 1;                //使能内部上拉,准备读取数据,
    for (i = 0; i < 8; i++) // 8位计数器
    {
        dat <<= 1;
        SCL = 1;    //拉高时钟线
        Delay5us(); //延时
        dat |= SDA; //读数据
        SCL = 0;    //拉低时钟线
        Delay5us(); //延时
    }
    return dat;
}

/***************************************************
 *@description  : BMP180 单字节写入BMP180内部EEPROM
 *@param         {unsigned char} SlaveAddress BMP180从机地址
 *@param         {unsigned char} REG_Address  内部寄存器地址
 *@param         {unsigned char} REG_data     内部寄存器欲写入数据
 *@return        {*}
 ***************************************************/
void Single_Write(unsigned char SlaveAddress, unsigned char REG_Address, unsigned char REG_data)
{
    BMP180_Start();                //起始信号
    BMP180_SendByte(SlaveAddress); //发送设备地址+写信号
    BMP180_SendByte(REG_Address);  //内部寄存器地址
    BMP180_SendByte(REG_data);     //内部寄存器数据
    BMP180_Stop();                 //发送停止信号
}

/***************************************************
 *@description  : BMP180内部EEPROM读取一个字节数据
 *@param         {unsigned char} REG_Address 内部寄存器地址
 *@return        {*}
 ***************************************************/
unsigned char Single_Read(unsigned char REG_Address)
{
    unsigned char REG_data;
    BMP180_Start();                           //起始信号
    BMP180_SendByte(BMP180_SlaveAddress);     //发送设备地址+写信号
    BMP180_SendByte(REG_Address);             //发送存储单元地址
    BMP180_Start();                           //起始信号
    BMP180_SendByte(BMP180_SlaveAddress + 1); //发送设备地址+读信号
    REG_data = BMP180_RecvByte();             //读出寄存器数据
    BMP180_SendACK(1);                        //非应答
    BMP180_Stop();                            //停止信号
    return REG_data;
}

/***************************************************
 *@description  : BMP180内部EEPROM读取连续的两字节数据
 *@param         {unsigned char} ST_Address 起始地址
 *@return        {*}
 ***************************************************/
short Multiple_read(unsigned char ST_Address)
{
    unsigned char msb, lsb;
    short _data;
    BMP180_Start();                           //起始信号
    BMP180_SendByte(BMP180_SlaveAddress);     //发送设备地址+写信号
    BMP180_SendByte(ST_Address);              //发送存储单元地址
    BMP180_Start();                           //起始信号
    BMP180_SendByte(BMP180_SlaveAddress + 1); //发送设备地址+读信号
    msb = BMP180_RecvByte();                  //接收数据
    BMP180_SendACK(0);                        //回应ACK
    lsb = BMP180_RecvByte();                  //接收数据
    BMP180_SendACK(1);                        //最后一个数据需要回NOACK
    BMP180_Stop();                            //停止信号
    Delay5ms();                               //延时
    _data = msb << 8;                         //处理数据
    _data |= lsb;                             //处理数据
    return _data;                             //返回
}

/***************************************************
 *@description  : BMP180 读取原始温度数据
 *@param         {*}
 *@return        {*}
 ***************************************************/
long BMP180ReadTemp(void)
{
    long Temperature;                        //温度数据
    BMP180_Start();                          //起始信号
    BMP180_SendByte(BMP180_SlaveAddress);    //发送设备地址+写信号
    BMP180_SendByte(0xF4);                   // write register address
    BMP180_SendByte(0x2E);                   // write register data for temp 180:0x2E 280:0x22
    BMP180_Stop();                           //发送停止信号
    delay(10);                               // max time is 4.5ms
    Temperature = (long)Multiple_read(0xF6); // 180=0xF6 280=0xFa
    return Temperature;
}

/***************************************************
 *@description  : BMP180 读取原始大气压数据
 *@param         {*}
 *@return        {*}
 ***************************************************/
unsigned int BMP180ReadPressure(void)
{
    unsigned int pressure = 0;
    BMP180_Start();                       //起始信号
    BMP180_SendByte(BMP180_SlaveAddress); //发送设备地址+写信号
    BMP180_SendByte(0xF4);                // write register address
    BMP180_SendByte(0x34);                // write register data for pressure 180=0x34 280=0x22
    BMP180_Stop();                        //发送停止信号
    delay(10);                            // max time is 4.5ms
    pressure = Multiple_read(0xF6);       // 180=0xF6 280=0xF9
    return pressure;
}

/***************************************************
 *@description  : 读出BMP180内部EEPROM寄存器中的校准系数
 *@param         {*}
 *@return        {*}
 ***************************************************/
void ReadCalibrationData(void)
{
    ac1 = Multiple_read(0xAA);
    ac2 = Multiple_read(0xAC);
    ac3 = Multiple_read(0xAE);
    ac4 = Multiple_read(0xB0);
    ac5 = Multiple_read(0xB2);
    ac6 = Multiple_read(0xB4);
    b1 = Multiple_read(0xB6);
    b2 = Multiple_read(0xB8);
    mb = Multiple_read(0xBA);
    mc = Multiple_read(0xBC);
    md = Multiple_read(0xBE);
}

/***************************************************
 *@description  : BMP180 转换数据，将原始数据转换成实际数据，并存在全局变量中
 *@param         {*}
 *@return        {*}
 ***************************************************/
void BMP180Convert(void)
{
    long ut;                        //  原始温度数据
    long up;                        //  原始压强数据
    long x1, x2, b5, b6, x3, b3, p; //  中间变量
    unsigned long b4, b7;           //  中间变量

    ut = BMP180ReadTemp(); // 读取温度
    delay(5);
    up = BMP180ReadPressure(); // 读取压强

    //  计算实际温度
    x1 = ((long)ut - ac6) * ac5 >> 15;
    x2 = ((long)mc << 11) / (x1 + md);
    b5 = x1 + x2;
    temperature = (b5 + 8) >> 4; //  实际温度数据

    //  计算实际压强
    b6 = b5 - 4000;
    x1 = (b2 * (b6 * b6 >> 12)) >> 11;
    x2 = ac2 * b6 >> 11;
    x3 = x1 + x2;
    b3 = (((long)ac1 * 4 + x3) + 2) / 4;
    x1 = ac3 * b6 >> 13;
    x2 = (b1 * (b6 * b6 >> 12)) >> 16;
    x3 = ((x1 + x2) + 2) >> 2;
    b4 = (ac4 * (unsigned long)(x3 + 32768)) >> 15;
    b7 = ((unsigned long)up - b3) * (50000 >> OSS);
    if (b7 < 0x80000000)
        p = (b7 * 2) / b4;
    else
        p = (b7 / b4) * 2;
    x1 = (p >> 8) * (p >> 8);
    x1 = (x1 * 3038) >> 16;
    x2 = (-7357 * p) >> 16;
    pressure = p + ((x1 + x2 + 3791) >> 4); //  实际压强数据

    //  海拔放大100倍，用厘米做单位，实际使用精确米，方便计算
    altitude = (long)(4433000.0 * (1.0 - pow((pressure * 1.0 / PRESSURE_0), (1.0 / 5.255))));// 单位：厘米
}
