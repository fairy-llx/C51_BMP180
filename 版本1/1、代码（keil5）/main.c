/***************************************************
 *@Author       : fairy
 *@Date         : 2022-03-13 09:25:49
*@LastEditors  : fairy
*@LastEditTime : 2022-03-13 11:24:32
*@FilePath     : main.c
 ***************************************************/
#include "LCD1602.h"
#include "BMP180.h"

void LCD1602Distemperature(unsigned char start_x, unsigned char start_y);
void LCD1602Dispressure(unsigned char start_x, unsigned char start_y);
void LCD1602Disaltitude(unsigned char start_x, unsigned char start_y);

void main(void)
{
    Init_LCD1602();
    ReadCalibrationData();

    LCD1602_Dis_OneChar(0, 0, 'T');
    LCD1602_Dis_OneChar(1, 0, ':');

    LCD1602_Dis_OneChar(0, 1, 'P');
    LCD1602_Dis_OneChar(1, 2, ':');

    while (1)
    {
        BMP180Convert();
        LCD1602Distemperature(2, 0);
        LCD1602Dispressure(2, 1);
        LCD1602Disaltitude(9, 0);
        delay(1000);
    }
}

void LCD1602Distemperature(unsigned char start_x, unsigned char start_y)
{
    //  BMP180 温度测量范围：-40~+85 摄氏度
    unsigned char ge, shi, bai;    //  温度各位数据
    unsigned char disbuf[8] = {0}; //  显示缓存
    unsigned char i = 0;

    if (temperature < 0)
    {
        disbuf[i++] = '-';
        temperature = -temperature;
    }
    ge = temperature % 10;
    shi = temperature / 10 % 10;
    bai = temperature / 100 % 10;

    if (temperature > 100)
    {
        disbuf[i++] = bai + '0';
        disbuf[i++] = shi + '0';
        disbuf[i++] = '.';
        disbuf[i++] = ge + '0';
    }
    else if (temperature > 10)
    {
        disbuf[i++] = shi + '0';
        disbuf[i++] = '.';
        disbuf[i++] = ge + '0';
    }
    else
    {
        disbuf[i++] = '0';
        disbuf[i++] = '.';
        disbuf[i++] = ge + '0';
    }
    disbuf[i++] = 0XDF;
    disbuf[i++] = 'C';

    LCD1602_Dis_Str(start_x, start_y, disbuf);
}

void LCD1602Dispressure(unsigned char start_x, unsigned char start_y)
{
    //  BMP180 大气压测量范围：300~1100hPa，即30~110kPa(30000~110000Pa)
    //  1 标准大气压=101.325 千帕 = 101.325 千帕

    unsigned char ge, shi, bai, qian, wan, shiwan; //   压强各位数值
    unsigned char Disbuf[11] = {0};                //   显示缓存
    unsigned char i = 0;

    ge = pressure % 10;
    shi = pressure / 10 % 10;
    bai = pressure / 100 % 10;
    qian = pressure / 1000 % 10;
    wan = pressure / 10000 % 10;
    shiwan = pressure / 100000 % 10;

    if (pressure > 100000)
    {
        Disbuf[i++] = shiwan + '0';
        Disbuf[i++] = wan + '0';
        Disbuf[i++] = qian + '0';
        Disbuf[i++] = '.';
        Disbuf[i++] = bai + '0';
        Disbuf[i++] = shi + '0';
        Disbuf[i++] = ge + '0';
    }
    else if (pressure > 10000)
    {
        Disbuf[i++] = wan + '0';
        Disbuf[i++] = qian + '0';
        Disbuf[i++] = '.';
        Disbuf[i++] = bai + '0';
        Disbuf[i++] = shi + '0';
        Disbuf[i++] = ge + '0';
    }
    Disbuf[i++] = 'k';
    Disbuf[i++] = 'P';
    Disbuf[i++] = 'a';
    // start_x = start_y;
    LCD1602_Dis_Str(start_x, start_y, Disbuf);
}

void LCD1602Disaltitude(unsigned char start_x, unsigned char start_y)
{
    //+9000m ... -500m 基于海平面
    unsigned char ge, shi, bai, qian, wan, shiwan;
    unsigned char disbuf[6] = {0};
    unsigned char i = 0;

    if (altitude < 0)
    {
        disbuf[i++] = '-';
        altitude = -altitude;
    }

    ge = altitude % 10;
    shi = altitude / 10 % 10;
    bai = altitude / 100 % 10;
    qian = altitude / 1000 % 10;
    wan = altitude / 10000 % 10;
    shiwan = altitude / 100000 % 10;

    if (altitude > 100000)
    {
        disbuf[i++] = shiwan + '0';
        disbuf[i++] = wan + '0';
        disbuf[i++] = qian + '0';
        disbuf[i++] = bai + '0';
    }
    else if (altitude > 10000)
    {
        disbuf[i++] = wan + '0';
        disbuf[i++] = qian + '0';
        disbuf[i++] = bai + '0';
    }
    else if (altitude > 1000)
    {
        disbuf[i++] = qian + '0';
        disbuf[i++] = bai + '0';
    }
    else
    {
        disbuf[i++] = bai + '0';
    }

    disbuf[i++] = 'm';

    LCD1602_Dis_Str(start_x, start_y, disbuf);
}