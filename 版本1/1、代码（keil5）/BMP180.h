/***************************************************
 *@Author       : fairy
 *@Date         : 2022-03-12 14:42:30
*@LastEditors  : fairy
*@LastEditTime : 2022-03-13 10:56:48
*@FilePath     : BMP180.h
 ***************************************************/

#ifndef __BMP180__
#define __BMP180__

#include <math.h>
#include <stdio.h>
#include <reg51.h>
#include <stdlib.h>
#include <INTRINS.H>
//  宏定义
#define BMP085_SlaveAddress 0xEE //定义器件在IIC总线中的从地址 BMP180=0xee,BMP280=0xec
#define OSS 0                    // Oversampling Setting (note: code is not set up to use other OSS values)

//  外部变量
extern long temperature; //  实际温度数据，单位：0.1 摄氏度
extern long pressure;    //  实际压强数据，单位：1   帕斯卡
extern long altitude;    // 根据气压计算出来的海拔高度，单位：1 厘米（计算方便）

//  函数声明

void Delay5ms(void);
void Delay5us(void);
void BMP180_Stop(void);
void BMP180_Start(void);
bit BMP180_RecvACK(void);
void BMP180Convert(void);
long BMP180ReadTemp(void);
void delay(unsigned int k);
void BMP180_SendACK(bit ack);
void ReadCalibrationData(void);
unsigned char BMP180_RecvByte(void);
unsigned int BMP180ReadPressure(void);
void BMP180_SendByte(unsigned char dat);
short Multiple_read(unsigned char ST_Address);
unsigned char Single_Read(unsigned char REG_Address);
void Single_Write(unsigned char SlaveAddress, unsigned char REG_Address, unsigned char REG_data);

#endif